//
//  ViewController.swift
//  MARKSHEET
//
//  Created by Click Labs 65 on 1/13/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var sub1: UITextField!
    
    
    @IBOutlet weak var sub2: UITextField!
    
    
    @IBOutlet weak var sub3: UITextField!
    
    
    @IBOutlet weak var sub4: UITextField!
    
    
    @IBOutlet weak var sub5: UITextField!
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var classField: UITextField!
    
    @IBOutlet weak var percentfield: UITextField!
    
    @IBOutlet weak var rollNo: UITextField!
    
    @IBOutlet weak var gradeField: UITextField!
    
    
    @IBOutlet weak var resultImage: UIImageView!
    
    @IBOutlet weak var infoLabel: UILabel!
    
    
    
    @IBAction func submitDetails(sender: AnyObject) {
        
        
        
        if ( nameField.text == "" || classField.text == "" || rollNo.text == "")
        {
            if nameField.text == "" {
                
                nameField.text = "Enter Name"
                
            }else if classField.text == "" {
                
                classField.text = "Enter class"
            
            }else if rollNo == ""
        {
            rollNo.text = "Enter roll number"
            
            }
        }else{}
        
        //checking for all subjects value filled or greater than 100 and directing user over there
        
        if ( sub1.text == "" || sub1.text.toInt() > 100  || sub2.text == "" || sub2.text.toInt() > 100 || sub3.text == "" || sub3.text.toInt() > 100 || sub4.text == "" || sub4.text.toInt() > 100 || sub5.text == "" || sub5.text.toInt() > 100 )
        {
            
            infoLabel.text = "Invalid data"
            
            if sub1.text == "" || sub1.text.toInt() > 100 {
                
                sub1 .becomeFirstResponder()
                
                sub1.text = "Enter Here"
            }
                
            else if sub2.text == "" || sub2.text.toInt() > 100 {
                
                sub2 .becomeFirstResponder()
                
                sub2.text = "Enter Here"
            }
                
            else if sub3.text == "" || sub3.text.toInt() > 100 {
                
                sub3 .becomeFirstResponder()
                
                sub3.text = "Enter Here"
            }
                
            else if sub4.text == "" || sub4.text.toInt() > 100 {
                
                sub4 .becomeFirstResponder()
                
                sub4.text = "Enter Here"
            }
                
            else if sub5.text == "" || sub5.text.toInt() > 100 {
                
                sub5.becomeFirstResponder()
                
                sub5.text = "Enter Here"
            }
            
            else {}
            
            percentfield.hidden = true
            
            gradeField.hidden = true
            
        }
            
    //if every text field is filled below code will be executed

        else{
            
            //assigning values
            
        
            var marks1 = (sub1.text as NSString).doubleValue
            
            var marks2 = (sub2.text as NSString).doubleValue
            
            var marks3 = (sub3.text as NSString).doubleValue
            
            var marks4 = (sub4.text as NSString).doubleValue
            
            var marks5 = (sub5.text as NSString).doubleValue
            
            var percentage = (marks1 + marks2 + marks3 + marks4 + marks5)/5
            //percentage calculation
            
            percentfield.hidden = false
            
            gradeField.hidden = false
            
            
            percentfield.text = "\(percentage)"
            
            //assignment of grades follow below with four cases
            
            var grade = [ "EXCELLENT", "VERY GOOD",  "AVERAGE", "FAIL"]
            
            
        if (percentage >= 25 && percentage < 50)
        {
            gradeField.text = "\(grade[2])"
            
            resultImage.image = UIImage(named: "2-Stars")
        }
        else if (percentage >= 50 && percentage < 75)
        {
            gradeField.text = "\(grade[1])"
            
            resultImage.image = UIImage(named: "3-Stars")
        }
        else if (percentage >= 75 && percentage <= 100)
        {
            gradeField.text = "\(grade[0])"
            
            resultImage.image = UIImage(named: "4-Stars")
        }
        else{
            gradeField.text = "\(grade[3])"
            
            resultImage.image = UIImage(named: "1star")
        }
            
            
        }
        
        
    }
    
    
    
    @IBAction func reset(sender: AnyObject) {
        
        nameField.text = ""
        
        classField.text = ""
        
        rollNo.text = ""
        
        sub1.text = ""
        
        sub2.text = ""
        
        sub3.text = ""
        
        sub4.text = ""
        
        sub5.text = ""
        
        percentfield.text = ""
        
        gradeField.text = ""
    }
    
    
    
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

